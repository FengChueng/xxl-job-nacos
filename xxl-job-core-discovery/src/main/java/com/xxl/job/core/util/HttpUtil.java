package com.xxl.job.core.util;

import com.alibaba.fastjson.JSON;

import java.util.Map;

/**
 * @author joybin
 */
public class HttpUtil {

    /**
     * json to invoke
     * @param url address
     * @param regJson json object
     * @return invoke result
     * @throws Exception
     */
    public static String sendHttpPost(String url, Object regJson) throws Exception {
        String str = JSON.toJSONString(regJson);
        return cn.hutool.http.HttpUtil.post(url, str);
    }

    /**
     * from param to invoke
     * @param url address
     * @param map param
     * @return invoke result
     * @throws Exception
     */
    public static String postFromParam(String url, Map<String, Object> map) {
        return cn.hutool.http.HttpUtil.post(url, map, 10000);
    }

}
